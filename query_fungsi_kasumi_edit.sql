

CREATE OR REPLACE FUNCTION public.process_product()
 RETURNS void
 LANGUAGE plpgsql
AS $function$
    BEGIN
        INSERT INTO master_data_product(product_id,product_tmpl_id,product_middleware_id,
									   	product_name_kana,product_name_kanji,product_code,standard_selling_price,
									   standard_value_supplier, standard_cost,product_categ, 
									   variety_categ,product_group_categ, department_categ,
									   need_create,need_update,product_categ_id,variety_categ_id,
									   product_group_categ_id,department_categ_id,vendor_id,
										product_jan_1,product_jan_2,product_jan_3)
        SELECT nextval('product_seq_id'), nextval('product_template_seq_id'), min(id) as product_middleware_id,
            min(product_name_kana), min(product_name_kanji),
            concat(department, product_code) as product_code,
            min(standard_selling_price) as standard_selling_price,
            min(standard_value_supplier) as standard_value_supplier,
            min(standard_cost) as standard_cost,
            min(concat(department, '/', product_group, '/', variety, '/', classification)) as product_categ,
            min(concat(department, '/', product_group, '/', variety)) as variety_categ,
            min(concat(department, '/', product_group)) as product_group_categ,
            min(concat(department)) as department_categ,
            TRUE as need_create,
			FALSE as need_update, 
			0 as product_categ_id, 
			0 as variety_categ_id, 
			0 as product_group_categ_id, 
			0 as department_categ_id, 
			0 as vendor_id, 
			min(scanning_code_1) as product_jan_1,
			min(scanning_code_2) as product_jan_2,
			min(scanning_code_3) as product_jan_3
        FROM product_master WHERE mark_as_done is FALSE
        GROUP BY concat(department, product_code)
            ON CONFLICT (product_code)
        DO UPDATE SET need_create = FALSE, need_update = TRUE,
            product_id = master_data_product.product_id,
            product_tmpl_id = master_data_product.product_tmpl_id,
            standard_selling_price = EXCLUDED.standard_selling_price,
            standard_value_supplier = EXCLUDED.standard_value_supplier,
            product_jan_1 = EXCLUDED.product_jan_1,
            product_jan_2 = EXCLUDED.product_jan_2,
            product_jan_3 = EXCLUDED.product_jan_3;

--         UPDATE master_data_product
--         SET
--             product_categ_id = pc.id
--         FROM product_category_odoo pc
--             WHERE product_categ = pc.unique_key
--             AND categ_type = 'classification';

--         UPDATE master_data_product
--         SET
--             variety_categ_id = pc.id
--         FROM product_category_odoo pc
--             WHERE variety_categ = pc.unique_key
--             AND categ_type = 'variety';

--         UPDATE master_data_product
--         SET
--             product_group_categ_id = pc.id
--         FROM product_category_odoo pc
--             WHERE product_group_categ = pc.unique_key
--             AND categ_type = 'product_group';

--         UPDATE master_data_product
--         SET
--             department_categ_id = pc.id
--         FROM product_category_odoo pc
--             WHERE department_categ = pc.unique_key
--             AND categ_type = 'department';


--         UPDATE master_data_product
--         SET
--             vendor_id = sp.id
--         FROM master_data_supplier sp
--             WHERE standard_value_supplier = sp.supplier_code;

--         UPDATE master_data_product SET product_categ_id = 1 WHERE product_categ_id is null;
        UPDATE product_master SET mark_as_done = TRUE;
    END;
$function$

--select public.process_product()

-- select * from master_data_product
-- where product_code = 
-- --where 
-- limit 100
